## HOW TO OPEN THIS PROJECT ON YOUR LOCAL MACHINE

**Process of downloading source code and running this web application on your local machine**

1. open up your terminal and navigate to the folder where you want to download the source code
2. copy the following code and paste it into your terminal
3. git clone `https://gitlab.com/darbhasailu/template.git`
4. a folder named template should appear now, you can check by using `ls` command to list all the folders if you're on Mac
5. navigate into the folder using terminal and then run the following command
6. `npm install`
7. after the installation is done run the following command
8. `npm run dev`
9. this will spin up the application on your machine at the following url
10. `http://localhost:5173`
