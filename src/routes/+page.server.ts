import type { Actions } from './$types';
import { redirect, error } from '@sveltejs/kit';

export const actions: Actions = {
	register: async ({ cookies, request }) => {
		const data = await request.formData();
		const email = data.get('email');
		const password = data.get('password');
		const password_confirmation = data.get('password-confirmation');
		console.log(data);

		//if (!email || !password || !password_confirmation) return { status: 400, required: true };

		if (password === password_confirmation) {
			// const user = await createUser(String(email), String(password), String(role));
			// const uuid = user.uuid;
			// setAuthenticationCookies(cookies, uuid);
			throw redirect(302, '/docchat');
		} else {
			throw error(400, 'passwords dont match');
		}

		throw redirect(302, '/dashboard');
	}
};
