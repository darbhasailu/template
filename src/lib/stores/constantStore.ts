export let INTROTEXT: string =
	'DocChat is a revolutionary online platform that seamlessly merges artificial intelligence and document handling to create a unique interaction experience.\n\n By leveraging advanced AI technologies, this innovative platform allows users to upload any document and initiate a conversation with it. The integrated AI chatbot interprets the document, analyzing its content to provide relevant responses.\n\n This not only simplifies data comprehension and information retrieval but also redefines document interaction, making it more interactive, intuitive, and accessible. Whether you are a professional dealing with complex files or a student wrestling with extensive academic materials, DocChat can transform your document interaction journey.';

import coursebook from '$lib/images/coursebook.png';
import coursebook2 from '$lib/images/coursebook2.png';
import coursebook3 from '$lib/images/coursebook3.png';
import coursebook4 from '$lib/images/coursebook4.png';
import coursebook5 from '$lib/images/coursebook5.png';

interface featureMessage {
	heading: string;
	primaryMessage: string;
	secondaryMessage: string;
	image: string;
}

interface folder {
	name: string;
	description: string;
	files: string[];
}

export let FEATURES: featureMessage[] = [
	{
		heading: 'FlashCards',
		primaryMessage:
			'Flashcards employ spaced repetition, a technique that optimizes memory retention. By reviewing flashcards at spaced intervals, you reinforce the information over time, allowing it to move from short-term memory to long-term memory.',
		secondaryMessage:
			'Flashcards encourage active learning, requiring you to actively recall information rather than passively reviewing it. This active engagement stimulates your brain, making the learning process more effective.',
		image: coursebook
	},
	{
		heading: 'Crosswords',
		primaryMessage:
			'Discover the transformative power of word games like crosswords! Unlock a world of knowledge, creativity, and inspiration. Engage your mind in a joyful pursuit that transcends mere entertainment your horizons.',
		secondaryMessage:
			'With each crossword solved, you sharpen your vocabulary, unravel the intricacies of language, and delve into the depths of new subjects. Embrace the challenge, for it is through adversity that we grow, and with each conquered clue, you emerge wiser and more confident.',
		image: coursebook2
	},
	{
		heading: 'Peer to Peer Testing',
		primaryMessage:
			'Embrace the power of peer-to-peer realtime testing, where collaboration meets knowledge and growth becomes limitless. Step into a world where learning transcends the solitary pursuit, and education transforms into a vibrant symphony of shared insights.',
		secondaryMessage:
			'You not only absorb knowledge but also actively contribute to the growth of others. As you engage in realtime testing with your peers, you forge connections that spark intellectual fireworks, igniting a collective brilliance that surpasses individual capabilities.',
		image: coursebook3
	},
	{
		heading: 'Advanced Search',
		primaryMessage:
			'Unleash the power of comprehensive search at your fingertips ! Our advanced search feature goes beyond mere file names, diving deep into the very essence of your documents, unlocking a treasure trove of knowledge within.',
		secondaryMessage:
			'No longer will you waste precious time sifting through countless folders or skimming through lengthy documents. Our search feature brings clarity and efficiency to your fingertips, guiding you to the exact information you need, precisely when you need it.',
		image: coursebook4
	},
	{
		heading: 'Collaboration & Communication',
		primaryMessage:
			'Witness the magic unfold in real-time as multiple minds merge, breathing life into a shared document. Simultaneous editing and commenting enable lively discussions, fostering a rich exchange of perspectives. With each interaction, the document evolves, becoming a true reflection of the collective brilliance of the team.',
		secondaryMessage:
			'No longer bound by physical limitations, our collaboration feature creates a virtual canvas where ideas flow freely, insights are shared instantaneously, and creativity knows no bounds.',
		image: coursebook5
	}
];

export let UPCOMINGFEATURES: featureMessage[] = [
	{
		heading: 'basil:android-solid',
		primaryMessage: 'Android Application Comin Soon',
		secondaryMessage: 'Native android application or flutter based application coming soon',
		image: 'basil:android-solid'
	},
	{
		heading: 'basil:apple-solid',
		primaryMessage: 'iOS Application Comin Soon',
		secondaryMessage:
			'Native iOS application based on SwiftUI or flutter based application coming soon',
		image: 'basil:apple-solid'
	},
	{
		heading: 'basil:asana-solid',
		primaryMessage: 'Asana Integration Comin Soon',
		secondaryMessage: 'Asana integration for managing tasks and working with files coming soon',
		image: 'basil:asana-solid'
	},
	{
		heading: 'basil:apps-solid',
		primaryMessage: 'Apps dashboard comin soon',
		secondaryMessage:
			'Gmail, Outlook, Zapier etc. integration for managing tasks and working with files coming soon',
		image: 'basil:apps-solid'
	}
];

export let folders: folder[] = [
	{
		name: 'IDGAF Iguanas',
		description: 'TanmayBhaat mota hai',
		files: ['I', 'II', 'III']
	},
	{
		name: 'Lovable Langoors',
		description: 'Langgor',
		files: ['one', 'two', 'three']
	},
	{
		name: 'Lovely DImple',
		description: '',
		files: ['1', '2']
	}
];
