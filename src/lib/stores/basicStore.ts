import { writable } from 'svelte/store';

export const notes = writable<string[]>([]);

export const notifications = writable<string[]>([]);

function removeToast() {
	notifications.update((state) => {
		state.shift();
		return state;
	});
}

export function toast(message: string) {
	notifications.update((state) => [message, ...state]);
	setTimeout(removeToast, 1500);
}
